# Getting started with REST API testing with Serenity and Cucumber 6

This tutorial show you how to get started with REST-API testing using Serenity and Cucumber 6. 

## Automation setup to get started
Git: Simply clone below project and import in Eclipse. voila!! And you are great to go.. 

    git clone https://gitlab.com/onkarb68/serenity_api_automation_framework.git
    cd serenity_api_automation_framework

# Project overview

This project gives you a basic project setup, along with some sample tests and supporting classes. 
The starter project shows , how one can validate, automate any API or feature end to end.

## 1. How to write reusable core API Automation framework.
 
	src/test/java/Utility/RestAssuredAPIFramework
			........->This class file is having all the CRUD API http calls wrapped under single umbrella. The advantage of doing this is easy to maintain single file VS multiple files.
			e.g. In future, if project business demands to automate API using latest version of rest assured, we only need to change this single java file rest all files will remain untouched. This kind of beauty and autonomy this framework brings on the table. 

## 2.  Where to write feature or page specific reusable methods which can be used anywhere?	
	src/test/java/petstore (petstoer assumed as one page or feature in product)
			.....->> This will have all the reusable methods, API calls which will be centrally managed at single place with "@Step" annotation. It will directly gets called in actual Step Definition by
			using  below syntax. 
			
			@Steps
	PetStoreAllCalls petStoreAllCalls;
	
## 3.  Where to write feature file & How?

	.		.......->src/test/resources/features/status/featurefile.feature

	Gherkin uses a set of special keywords to give structure and meaning to executable specifications. 
	Keywords:
		The primary keywords are:

			1.Feature
			2.Given, When, Then, And, But for steps (or *)
			3.Background
			4.Scenario Outline (or Scenario Template)
			5.Examples (or Scenarios)
			
		There are a few secondary keywords as well:

			1.""" (Doc Strings)
			2.| (Data Tables)
			3.@ (Tags)
			4.# (Comments)
	
	Feature:
	The purpose of the Feature keyword is to provide a high-level description of a software feature, and to group related scenarios.
	
	### A simple GET scenario
		The project comes with scenarios which illustrates all CRUD operations like GET , POST , PUT & DELETE

		The first scenario exercises the `/api/available` endpoint
		
		Scenario Outline: Verify GET APIs of Petstore
			Given The petstore application server is running
			When User find the pet by its "<petID>"
			Then .....some actions etc etc..

		```Gherkin
		  Scenario: Application status end-point
			Given the application is running
			When I check the application status
			Then .....some actions etc etc..

## 4.  How to write step definition? 
	
		The glue code is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls.
	
	Location: src/test/java/starter/stepdefinitions/fileName.java
	
    @Steps
	RestAssuredAPIFramework restAssuredAPIFramework;

	@Steps
	PetStoreAllCalls petStoreAllCallsStepdef;

    	// Validates GET call positive scenario
	@When("User find the pet by its {string}")
	public void user_find_the_pet_by_its(String petNumber) {

		logger.info("Testing Start: User finds the pet by its valid id");

		Assert.assertEquals("unavailable", petStoreAllCallsStepdef.getPetByItsStaus(petNumber));

		logger.info("Testing Finish successfully: User finds the pet by its valid id");
	}

	// Validates GET call negative scenario
	@Then("User tries to find the pet by its {string} negative value")
	public void user_tries_to_find_the_pet_by_its_negative_value(String petNumber) {

		logger.info("Testing Start: User tries to find the pet by its negative value");
		
		Assert.assertNotEquals("null", petStoreAllCallsStepdef.getPetByItsStaus(petNumber));

		logger.info("Testing Finish : User tries to find the pet by its negative value");
	}

## 5.  How to group reusable feature specific components?

	Location: src/test/java/petstore/PetStoreAllCalls.java
	
	The actual REST calls are performed using RestAssured in the action classes, like `PetStoreAllCalls` here. 
	These use internally consuming core http calls from REST Utility. This will have reusable feature specific components or methods which can be consumed anywhere, which helps to make Automation code more modular.

	public class PetStoreAllCalls {
   @Steps
	RestAssuredAPIFramework restAssuredAPIFramework;
	
	@Step("Get current server status")
	    public  int readStatusMessage() {
		 return restAssuredAPIFramework.getResponseStatus(restAssuredAPIFramework.getServerAPIEndpoint());
	    }
	
	@Step("Get pet by petID")
    public String getPetByItsStaus(String petNo) {
		Response response=restAssuredAPIFramework.getResponse(restAssuredAPIFramework.getServerAPIEndpoint()+ getPet + petNo);
		return response.jsonPath().getString("status");
    }
	}

## 6. How to perform data driven testing using cucumber data tables?
	
	In many cases, these scenarios require mock data to exercise a feature, which can be cumbersome to inject — especially with complex or multiple entries.
	
	Feature file:
	
		Scenario: Verify POST APIs of Petstore using data table
		Given The petstore application server is running
		When  User creates the pet with petID
		 |1111		| 2222 |
		When  User creates the pet with negativepetID negative number
		 |-77			| abc	 |
		 
	Step definition file:
	
		@When("User creates the pet with petID")
	public void user_creates_the_pet_with_pet_id(io.cucumber.datatable.DataTable positiveDataTable) {

		logger.info("Testing Start: User creates the pet with valid petID");

		List<List<String>> rows = positiveDataTable.asLists(String.class);

		for (int i = 0; i < rows.size(); i++) {

			Assert.assertEquals(200, petStoreAllCalls.createPet(rows.get(0).get(i)).getStatusCode());
			Assert.assertEquals(rows.get(0).get(i),
					petStoreAllCalls.createPet(rows.get(0).get(i)).jsonPath().getString("id"));
			Assert.assertEquals("available", petStoreAllCalls.getPetByItsStaus(rows.get(0).get(i)));
		}

		logger.info("Testing Finish successfully: User creates the pet with petID");
		
	}

## 7. How to generate Test execution reports?
	
	You can generate full Serenity reports by running mvn clean verify
	
		Location:  /serenity_api_automation_framework/target/site/serenity/index.html
		
## Gitlab CI
	
	GitLab CI (Continuous Integration) service is a part of GitLab that build and test the software whenever developer pushes code to application.

	.gitlab-ci.yml
	
		demo_job_1:
     tags:
       - ci
     script:
      - mvn clean verify
	