package Utility;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class RestAssuredAPIFramework {
	Logger logger = Logger.getLogger("RestAssuredAPIFramework");
	
	//Method is used to get the response code
	@Step("Getting API status code")
	public int getResponseStatus(String apiEndPoint) {
		int statusCode = given().when().get(apiEndPoint).getStatusCode();
		logger.info("Status code is: " +statusCode);
		return statusCode;
	}

	//Method is used to get the response
	@Step("Getting server response with parameters")
	public Response getResponse(String apiEndPoint, Map<String, ?> parametersMap) {

		RestAssured.baseURI = getServerAPIEndpoint();
		logger.info("Base URI is: " +RestAssured.baseURI);
		RequestSpecification httpRequest = RestAssured.given();
		// Response response = httpRequest.get(apiEndPoint);
		Response response = httpRequest.params(parametersMap).get(apiEndPoint);
		return response;

	}
	
	//Method is used to get the response
	@Step("Getting server response without parameters")
	public Response getResponse(String apiEndPoint) {

		RestAssured.baseURI = getServerAPIEndpoint();
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.get(apiEndPoint);
		return response;
	}

	//Method is used to get the Server Endpoint URI
	@Step("Getting server end point")
	@SuppressWarnings("unused")
	public String getServerAPIEndpoint() {

		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("src//test//resources//webservices.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("API_ENDPOINTis: " +RestAssured.baseURI);
		return prop.getProperty("API_ENDPOINT");

	}

	
	//Method is used to trigger the POST call
	@Step("POST call")
	public Response postCall(String requestBody, String postAPI) {
		RestAssured.baseURI = getServerAPIEndpoint();
		Response response = given().header("Content-type", "application/json").and().body(requestBody).when()
				.post(postAPI).then().extract().response();

		return response;

	}
	
	//Method is used to trigger the PUT call
	@Step("PUT call")
	public Response putCall(String requestBody, String putAPI) {
		RestAssured.baseURI = getServerAPIEndpoint();
		Response response = given().header("Content-type", "application/json").and().body(requestBody).when()
				.put(putAPI).then().extract().response();

		return response;

	}
	
	//Method is used to trigger the DELETE call
	@Step("DELETE call")
	public Response deleteCall(Map<String, ?> requestHeader,String deleteAPI) {

		RestAssured.baseURI =deleteAPI;
		System.out.println("deleteAPI  "+deleteAPI);
		Response response = given()
                .headers("Content-type","application/json")
                .headers("apiKey","a951ca51-4db0-4532-849b-ae9fdf12edfa")
                .when()
                .delete(deleteAPI)
                .then()
                .extract().response();

		return response;

	}
}
