package petstore;

import java.util.HashMap;
import java.util.Map;

import Utility.RestAssuredAPIFramework;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;


/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class PetStoreAllCalls {
	
	String getPet="v2/pet/";
	String createPet="v2/pet";
	String deletePet="v2/pet/";
	
	@Steps
	RestAssuredAPIFramework restAssuredAPIFramework;

	
	@Step("Get current server status")
	    public  int readStatusMessage() {
		 return restAssuredAPIFramework.getResponseStatus(restAssuredAPIFramework.getServerAPIEndpoint());
	    }
	
	@Step("Get pet by petID")
    public String getPetByItsStaus(String petNo) {
		Response response=restAssuredAPIFramework.getResponse(restAssuredAPIFramework.getServerAPIEndpoint()+ getPet + petNo);
		return response.jsonPath().getString("status");
    }
	
	@Step("Create pet by petID")
	public Response createPet(String petID) {
		String payload ="{\r\n"
				+ "  \"id\": "+petID+",\r\n"
				+ "  \"category\": {\r\n"
				+ "    \"id\": 0,\r\n"
				+ "    \"name\": \"string\"\r\n"
				+ "  },\r\n"
				+ "  \"name\": \"doggie\",\r\n"
				+ "  \"photoUrls\": [\r\n"
				+ "    \"string\"\r\n"
				+ "  ],\r\n"
				+ "  \"tags\": [\r\n"
				+ "    {\r\n"
				+ "      \"id\": 0,\r\n"
				+ "      \"name\": \"string\"\r\n"
				+ "    }\r\n"
				+ "  ],\r\n"
				+ "  \"status\": \"available\"\r\n"
				+ "}";
	    
		Response response=restAssuredAPIFramework.postCall(payload,createPet);
		return response;	
	}
	
	@Step("Update pet by petID")
	public Response updatePet(String latestpetID) {
		String payload ="{\r\n"
				+ "  \"id\": "+latestpetID+",\r\n"
				+ "  \"category\": {\r\n"
				+ "    \"id\": 0,\r\n"
				+ "    \"name\": \"string\"\r\n"
				+ "  },\r\n"
				+ "  \"name\": \"doggie\",\r\n"
				+ "  \"photoUrls\": [\r\n"
				+ "    \"string\"\r\n"
				+ "  ],\r\n"
				+ "  \"tags\": [\r\n"
				+ "    {\r\n"
				+ "      \"id\": 0,\r\n"
				+ "      \"name\": \"string\"\r\n"
				+ "    }\r\n"
				+ "  ],\r\n"
				+ "  \"status\": \"unavailable\"\r\n"
				+ "}";
	    
		Response response=restAssuredAPIFramework.putCall(payload,createPet);
		return response;	
	}
	 
	@Step("Delete pet by petID")
    public int deletePetById(String petNo) {	
		
		String deleteURI=restAssuredAPIFramework.getServerAPIEndpoint()+ deletePet + petNo;
		Map<String,String> queryHeader = new HashMap<>();
		queryHeader.put("apiKey", "a951ca51-4db0-4532-849b-ae9fdf12edfa");
		queryHeader.put("Content-type", "application/json");
		
		Response response=restAssuredAPIFramework.deleteCall(queryHeader,deleteURI);
		
		return response.getStatusCode();
    }
}
