package starter;

public enum WebServiceEndPoints {
    STATUS("https://petstore.swagger.io/"),
    TRADE("http://localhost:8080/api/trade");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
