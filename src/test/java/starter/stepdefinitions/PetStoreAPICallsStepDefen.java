package starter.stepdefinitions;

import org.apache.log4j.Logger;
import org.junit.Assert;

import Utility.RestAssuredAPIFramework;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import petstore.PetStoreAllCalls;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class PetStoreAPICallsStepDefen {

	Logger logger = Logger.getLogger("PetStoreAPICallsStepDefen");

	@Steps
	RestAssuredAPIFramework restAssuredAPIFramework;

	@Steps
	PetStoreAllCalls petStoreAllCalls;

	// Check status of API server
	@Given("The petstore application server is running")
	public void the_application_is_running() {

		Assert.assertEquals(200, petStoreAllCalls.readStatusMessage());

	}

	// Validates POST call positive scenario
	@When("User creates the pet with {string}")
	public void user_creates_the_pet_with(String petNumber) {

		logger.info("Testing Start: User creates the pet with valid petID");

		Assert.assertEquals(200, petStoreAllCalls.createPet(petNumber).getStatusCode());
		Assert.assertEquals(petNumber, petStoreAllCalls.createPet(petNumber).jsonPath().getString("id"));
		Assert.assertEquals("available", petStoreAllCalls.getPetByItsStaus(petNumber));

		logger.info("Testing Finish successfully: User creates the pet with valid petID");


	}

	// Validates POST call negative scenario
	@Then("User creates the pet with {string} negative number")
	public void user_creates_the_pet_with_negative_number(String petNumber) {

		logger.info("Testing Start:Creation of pet with negativepetID");

		Assert.assertNotEquals(petNumber, petStoreAllCalls.createPet(petNumber).jsonPath().getString("id"));

		logger.info("Testing Finish : Creation of pet with negativepetID");

	}

	// Validates GET call positive scenario
	@When("User find the pet by its {string}")
	public void user_find_the_pet_by_its(String petNumber) {

		logger.info("Testing Start: User finds the pet by its valid id");

		Assert.assertEquals("unavailable", petStoreAllCalls.getPetByItsStaus(petNumber));

		logger.info("Testing Finish successfully: User finds the pet by its valid id");
	}

	// Validates GET call negative scenario
	@Then("User tries to find the pet by its {string} negative value")
	public void user_tries_to_find_the_pet_by_its_negative_value(String petNumber) {

		logger.info("Testing Start: User tries to find the pet by its negative value");
		
		Assert.assertNotEquals("null", petStoreAllCalls.getPetByItsStaus(petNumber));

		logger.info("Testing Finish : User tries to find the pet by its negative value");
	}

	// Validates PUT call positive scenario
	@When("User updates the pet status to unavilable with {string}")
	public void user_updates_the_pet_with_having_id(String petId) {

		logger.info("Testing Start: User updates the pet status to unavilable");
		
		Assert.assertEquals(200, petStoreAllCalls.updatePet(petId).getStatusCode());
		Assert.assertEquals("unavailable", petStoreAllCalls.updatePet(petId).jsonPath().getString("status"));

		logger.info("Testing Finish successfully: User updates the pet status to unavilable");
	}

	// Validates DELETE call positive scenario
	@When("User tries to delete valid pet by its {string}")
	public void user_tries_to_delete_valid_pet_by_its(String petId) {

		logger.info("Testing Start: User tries to delete valid pet");
		
		Assert.assertEquals(200, petStoreAllCalls.deletePetById(petId));

		logger.info("Testing Finish successfully: User tries to delete valid pet");
	}

	// Validates DELETE call negative scenario
	@Then("User tries to delete invalid pet by its {string}")
	public void user_tries_to_delete_invalid_pet_by_its(String petId) {

		logger.info("Testing Start: User tries to delete invalid pet");

		Assert.assertEquals(404, petStoreAllCalls.deletePetById(petId));

		logger.info("Testing Finish : User tries to delete invalid pet");
	}

}
