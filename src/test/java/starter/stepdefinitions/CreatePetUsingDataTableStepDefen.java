package starter.stepdefinitions;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;

import Utility.RestAssuredAPIFramework;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import petstore.PetStoreAllCalls;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class CreatePetUsingDataTableStepDefen {

	Logger logger = Logger.getLogger("CreatePetUsingDataTableStepDefen");

	@Steps
	RestAssuredAPIFramework restAssuredAPIFramework;

	@Steps
	PetStoreAllCalls petStoreAllCalls;

	// Validates POST call positive scenario
	@When("User creates the pet with petID")
	public void user_creates_the_pet_with_pet_id(io.cucumber.datatable.DataTable positiveDataTable) {

		logger.info("Testing Start: User creates the pet with valid petID");

		List<List<String>> rows = positiveDataTable.asLists(String.class);

		for (int i = 0; i < rows.size(); i++) {

			Assert.assertEquals(200, petStoreAllCalls.createPet(rows.get(0).get(i)).getStatusCode());
			Assert.assertEquals(rows.get(0).get(i),
					petStoreAllCalls.createPet(rows.get(0).get(i)).jsonPath().getString("id"));
			Assert.assertEquals("available", petStoreAllCalls.getPetByItsStaus(rows.get(0).get(i)));
		}

		logger.info("Testing Finish successfully: User creates the pet with petID");
		
	}

	// Validates POST call negative scenario
	@When("User creates the pet with negativepetID negative number")
	public void user_creates_the_pet_with_negativepet_id_negative_number(
			io.cucumber.datatable.DataTable negativeDataTable) {

		logger.info("Testing Start:Creation of pet with negativepetID");

		List<List<String>> rows = negativeDataTable.asLists(String.class);

		for (int i = 0; i < rows.size(); i++) {

			Assert.assertNotEquals(rows.get(0).get(i),
					petStoreAllCalls.createPet(rows.get(0).get(i)).jsonPath().getString("id"));
		}

		logger.info("Testing Finish : Creation of pet with negativepetID");
	}

}
