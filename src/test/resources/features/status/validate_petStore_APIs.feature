Feature: All Petstore APIs i.e. POST, GET, UPDATE , DELETE , PATCH

  Scenario Outline: Verify POST APIs of Petstore
    Given The petstore application server is running
    When User creates the pet with "<petID>"
    When User creates the pet with "<negativepetID>" negative number

    Examples: 
      | petID | negativepetID |
      |  1111 |           -77 |
      |  2222 | abc           |

  Scenario Outline: Verify PUT APIs of Petstore
    Given The petstore application server is running
    When User updates the pet status to unavilable with "<petID>"

    Examples: 
      | petID |
      |  1111 |
      |  2222 |

  Scenario Outline: Verify GET APIs of Petstore
    Given The petstore application server is running
    When User find the pet by its "<petID>"
    When User tries to find the pet by its "<negativepetID>" negative value

    Examples: 
      | petID | negativepetID |
      |  1111 | xyz           |
      |  2222 |            -2 |

  Scenario Outline: Verify DELETE APIs of Petstore
    Given The petstore application server is running
    When User tries to delete valid pet by its "<petID>"
    Then User tries to delete invalid pet by its "<negativepetID>"

    Examples: 
      | petID | negativepetID |
      |  1111 |           -77 |
      |  2222 | abc           |
