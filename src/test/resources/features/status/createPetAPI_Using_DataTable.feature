Feature: Create the pet using POST API. Data table are being used for data driven testing.

  Scenario: Verify POST APIs of Petstore using data table
    Given The petstore application server is running
    When User creates the pet with petID
      | 1111 | 2222 |
    When User creates the pet with negativepetID negative number
      | -77 | abc |
